/**
 * BulkLazyload is a plugin inspirated by lazyloader under WTFPL
 *
 * @licence WTFPL 2018
 * @author Golga <r-ro@bulko.net> for Bulko
 *
 * Dependecies
 * ES6, Jquery 1 || 2 || 3
 *
 * How to use it
 *
 * In your main js file import this file
 * `import BulkLazyload from './{YOUR-PATH}/bulkLazyload';`
 * 
 * Then init this plugin in your document.ready (or ready function for turbolinks users)
 * `let bulkLazyload = new BulkLazyload();`
 * `bulkLazyload.init();
 */
export default class BulkLazyload
{
	init()
	{
		this.lazywatcher();

	}
	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since 1.1.0 ( 2018-07-09 )
	 *
	 * @return Void
	 */
	lazywatcher()
	{
		let bulkLazyload = this;
		$(window).on('scroll', function()
		{
			bulkLazyload.loadwithlazyiness();
		});
		$(window).on('hashchange', function (e) {
			bulkLazyload.loadwithlazyiness();
		});
		bulkLazyload.loadwithlazyiness();
	}

	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since 1.2.3 ( 2018-07-12 )
	 *
	 * @return Void
	 */
	loadwithlazyiness()
	{
		let bulkLazyload = this;
		$('.lazyload').each(function()
		{
			let elem = this;
			if ( bulkLazyload.checkVisible( elem ) )
			{
				bulkLazyload.lazyload( $(elem) );
			}
		});
	};

	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since 1.2.3 ( 2018-07-12 )
	 *
	 * @param jQueryObj $elem
	 * @return Void
	 */
	lazyload ( $elem )
	{
		if(
			$elem.prop("tagName") === "img"
			|| $elem.prop("tagName") === "IMG"
			|| $elem.prop("tagName") === "iframe"
			|| $elem.prop("tagName") === "IFRAME"
		)
		{
			$elem.attr('src', $elem.attr("data-lazy"));
		}
		else
		{
			$elem.css( "background-image", "url(" + $elem.attr("data-lazy") + ")" );
		}
		$elem.removeClass('lazyload');
	}

	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since Legacy
	 *
	 * @param String elem (selector)
	 * @param String evalType
	 * @return Bool
	 */
	checkVisible( elem, evalType )
	{
		evalType = evalType || "visible";

		let vpH = $(window).height(), // Viewport Height
			st = $(window).scrollTop(), // Scroll Top
			y = $(elem).offset().top,
			elementHeight = $(elem).height();
		if ( evalType === "visible" )
		{
			return ( ( y < ( vpH + st ) ) && ( y > ( st - elementHeight ) ) );
		}
		else if ( evalType === "above" )
		{
			return ( ( y < ( vpH + st ) ) );
		}
		return false;
	}
}
